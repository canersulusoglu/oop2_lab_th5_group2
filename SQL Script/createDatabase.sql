USE [master]
GO
/****** Object:  Database [db_a75663_database]    Script Date: 12.06.2021 23:19:53 ******/
CREATE DATABASE [db_a75663_database]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_a75663_database_Data', FILENAME = N'H:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_a75663_database_DATA.mdf' , SIZE = 8192KB , MAXSIZE = 1024000KB , FILEGROWTH = 10%)
 LOG ON 
( NAME = N'db_a75663_database_Log', FILENAME = N'H:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_a75663_database_Log.LDF' , SIZE = 3072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [db_a75663_database] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_a75663_database].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_a75663_database] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_a75663_database] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_a75663_database] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_a75663_database] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_a75663_database] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_a75663_database] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_a75663_database] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_a75663_database] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_a75663_database] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_a75663_database] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_a75663_database] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_a75663_database] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_a75663_database] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_a75663_database] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_a75663_database] SET  ENABLE_BROKER 
GO
ALTER DATABASE [db_a75663_database] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_a75663_database] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_a75663_database] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_a75663_database] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_a75663_database] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_a75663_database] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_a75663_database] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_a75663_database] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_a75663_database] SET  MULTI_USER 
GO
ALTER DATABASE [db_a75663_database] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_a75663_database] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_a75663_database] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_a75663_database] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [db_a75663_database] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [db_a75663_database] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [db_a75663_database] SET QUERY_STORE = OFF
GO
USE [db_a75663_database]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 12.06.2021 23:19:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[price] [float] NOT NULL,
	[description] [varchar](250) NULL,
	[image_path] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ShoppingCarts]    Script Date: 12.06.2021 23:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShoppingCarts](
	[user_id] [int] NOT NULL,
	[product_id] [int] NOT NULL,
	[amount] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 12.06.2021 23:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[phone_number] [varchar](50) NULL,
	[address] [varchar](100) NULL,
	[password] [varchar](50) NULL,
	[profile_image_path] [varchar](50) NULL,
	[rank] [int] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (4, N'Apple', 0.5, N'Elma', N'apple.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (5, N'Pear', 0.699999988079071, N'Armut', N'pear.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (6, N'Orange', 1, N'Portakal', N'orange.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (7, N'Peach', 1.1000000238418579, N'Seftali', N'peach.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (8, N'Banana', 2, N'Muz', N'banana.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (9, N'Pineapple', 5, N'Ananas', N'pineapple.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (10, N'Strawberry', 7.5999999046325684, N'Çilek', N'strawberry.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (11, N'Watermelon', 6, N'Karpuz', N'watermelon.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (12, N'Melon', 4.3000001907348633, N'Kavun', N'melon.jpg')
INSERT [dbo].[Products] ([id], [name], [price], [description], [image_path]) VALUES (13, N'Kiwi', 15, N'Kivi', N'kiwi.jpg')
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([id], [username], [email], [phone_number], [address], [password], [profile_image_path], [rank]) VALUES (13, N'adminuser', N'admin@gmail.com', N'9876543', N'admin address', N'123456', N'test.jpg', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [unique_email]    Script Date: 12.06.2021 23:20:00 ******/
CREATE UNIQUE NONCLUSTERED INDEX [unique_email] ON [dbo].[Users]
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [unique_username]    Script Date: 12.06.2021 23:20:00 ******/
CREATE UNIQUE NONCLUSTERED INDEX [unique_username] ON [dbo].[Users]
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_price]  DEFAULT ((0)) FOR [price]
GO
ALTER TABLE [dbo].[ShoppingCarts] ADD  CONSTRAINT [DF_ShoppingCarts_amount]  DEFAULT ((0)) FOR [amount]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_rank]  DEFAULT ((0)) FOR [rank]
GO
USE [master]
GO
ALTER DATABASE [db_a75663_database] SET  READ_WRITE 
GO
