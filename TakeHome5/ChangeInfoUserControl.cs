﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome5
{
    public partial class ChangeInfoUserControl : UserControl
    {
        OpenFileDialog dialog;
        private bool isAdressChanged = false;
        private bool isPhoneNumberChanged = false;
        private bool isProfilePictureChanged = false;

        public ChangeInfoUserControl()
        {
            dialog = new OpenFileDialog();
            InitializeComponent();
        }

        private void ChangeInfoUserControl_Load(object sender, EventArgs e)
        {
            var user = Database.getInstance().getLoggedUser();
            pictureBoxProfile.Image = Image.FromFile(user.ProfileImageWithPath);
            textBoxPhoneNumber.Text = user.PhoneNumber;
            textBoxAdress.Text = user.Address;
            lblUserName.Text = user.UserName;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!isAdressChanged && !isPhoneNumberChanged && !isProfilePictureChanged)
            {
                MessageBox.Show("Please change any information to update!");
            }
            else
            {
                bool result = false;
                if (isAdressChanged)
                {
                    result = Database.getInstance().changeAddress(textBoxAdress.Text);
                    
                }
                if (isPhoneNumberChanged)
                {
                    result = Database.getInstance().changePhoneNumber(textBoxPhoneNumber.Text);
                }
                if (isProfilePictureChanged)
                {
                    bool isSuccess = Database.getInstance().changeProfileImage(dialog.SafeFileName);
                    if (isSuccess)
                    {
                        int userId = Database.getInstance().getLoggedUser().Id;
                        string destinationPath = Path.Combine(Program.UsersImagesDir, userId.ToString(), dialog.SafeFileName);
                        File.Copy(dialog.FileName, destinationPath, true);
                    }
                }
                if (result)
                {
                    MessageBox.Show("Your profile successfully updated");
                }
                else
                {
                    MessageBox.Show("Something went wrong");
                }
            }
                

        }

        private void textBoxPhoneNumber_TextChanged(object sender, EventArgs e)
        {
            isPhoneNumberChanged = true;
        }

        private void textBoxAdress_TextChanged(object sender, EventArgs e)
        {
            isAdressChanged = true;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            dialog.Filter = "Image Files(*.jpg; *.jpeg *.gif; *.bmp;)|*.jpg;*.jpeg; *.gif;*.bmp;";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pictureBoxProfile.Image = Image.FromFile(dialog.FileName);
                isProfilePictureChanged = true;
            }
        
        }
    }
}
