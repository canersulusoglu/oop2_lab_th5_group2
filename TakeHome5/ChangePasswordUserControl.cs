﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome5
{
    public partial class ChangePasswordUserControl : UserControl
    {
        public ChangePasswordUserControl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (currentPasswordText.Text != "" && newPasswordText.Text != "" && NewPasswordTextAgain.Text != "")
            {
                if (Database.getInstance().getLoggedUser().Password == currentPasswordText.Text)
                {
                    if (newPasswordText.Text == NewPasswordTextAgain.Text)
                    {
                        Database.getInstance().changePassword(newPasswordText.Text);
                    }
                    else
                    {
                        MessageBox.Show("New password areas are not the same!");
                    }
                }
                else
                {
                    MessageBox.Show("Current password is not true");
                }
            }
            else
            {
                MessageBox.Show("Please fill all areas");
            }
        }
    }
}
