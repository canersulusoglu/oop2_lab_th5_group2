﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace TakeHome5
{
    class Database
    {
        private SqlConnection SqlConnection;

        private bool isUserLogged = false;
        private User loggedUser;
        public List<Product> Products = new List<Product>();
        private Dictionary<Product, int> ShoppingCart = new Dictionary<Product, int>();

        private static Database instance;
        private static object lockObject = new Object();
        private Database() {
            DatabaseConnection();
        }

        public static Database getInstance()
        {
            lock (lockObject)
            {
                if (instance == null)
                {
                    instance = new Database();
                }
                return instance;
            }
        }

        #region About Database Connection

        private void DatabaseConnection()
        {
            var connectionString = @"Data Source=SQL5063.site4now.net;Initial Catalog=db_a75663_database;User Id=db_a75663_database_admin;Password=Database123";
            SqlConnection = new SqlConnection(connectionString);
        }

        private void OpenConnection()
        {
            SqlConnection.Open();
        }

        private void CloseConnection()
        {
            SqlConnection.Close();
        }
        #endregion

        #region About Products

        public Product getProduct(int productId)
        {
            try
            {
                OpenConnection();
                string query = @"Select * from Products where id=" + productId.ToString();
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                DataTable dataTable = new DataTable();
                command.Fill(dataTable);
                CloseConnection();
                if (dataTable.Rows.Count == 1)
                {
                    DataRow item = dataTable.Rows[0];
                    return new Product(
                            int.Parse(item["id"].ToString()),
                            item["name"].ToString(),
                            float.Parse(item["price"].ToString()),
                            item["description"].ToString(),
                            item["image_path"].ToString()
                        );
                }
                return null;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        public List<Product> getProducts()
        {
            try
            {
                OpenConnection();
                string query = @"Select * from Products";
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                DataTable dataTable = new DataTable();
                command.Fill(dataTable);
                CloseConnection();
                Products.Clear();
                foreach (DataRow item in dataTable.Rows)
                {
                    Products.Add(new Product(
                        int.Parse(item["id"].ToString()),
                        item["name"].ToString(),
                        float.Parse(item["price"].ToString()),
                        item["description"].ToString(),
                        item["image_path"].ToString()
                    ));
                }
                return Products;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return null;
        }

        public Product findProductById(int id)
        {
            return Products.Find((x) => x.Id == id);
        }

        public int addProduct(Product newProduct)
        {
            try
            {
                OpenConnection();
                string query = @"Insert into Products(name,price,description,image_path) values(@name,@price,@description,@image_path) Select CAST(scope_identity() AS int)";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@name", newProduct.Name);
                command.Parameters.AddWithValue("@price", newProduct.Price);
                command.Parameters.AddWithValue("@description", newProduct.Desc);
                command.Parameters.AddWithValue("@image_path", newProduct.ImageName);
                int addedProductId = (int)command.ExecuteScalar();
                CloseConnection();
                MessageBox.Show("Product added.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Products.Add(new Product(addedProductId, newProduct.Name, newProduct.Price, newProduct.Desc, newProduct.ImageName));
                return addedProductId;
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return -1;
        }

        public void updateProduct(Product updateProduct)
        {
            try
            {
                OpenConnection();
                string queryUpdate = @"Update Products set name=@name,price=@price,description=@description,image_path=@image_path where id=@productId";
                SqlCommand commandUpdate = new SqlCommand(queryUpdate, SqlConnection);
                commandUpdate.Parameters.AddWithValue("@name", updateProduct.Name);
                commandUpdate.Parameters.AddWithValue("@price", updateProduct.Price);
                commandUpdate.Parameters.AddWithValue("@description", updateProduct.Desc);
                commandUpdate.Parameters.AddWithValue("@image_path", updateProduct.ImageName);
                commandUpdate.Parameters.AddWithValue("@productId", updateProduct.Id);
                commandUpdate.ExecuteNonQuery();
                CloseConnection();

                MessageBox.Show("Product updated.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void deleteProductById(int id)
        {
            try
            {
                OpenConnection();
                string query = @"Delete from Products where id=@productId";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@productId", id);
                command.ExecuteNonQuery();
                CloseConnection();
                MessageBox.Show("Product has been deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Products.Remove(findProductById(id));
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Products.Remove(this.findProductById(id));
        }

        #endregion

        #region About Shopping Cart

        public void addShoppingCart(int productId)
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string query = @"Select COUNT(*) From ShoppingCarts where user_id=@userId and product_id=@productId";
                    SqlCommand command = new SqlCommand(query, SqlConnection);
                    command.Parameters.AddWithValue("@userId", loggedUser.Id);
                    command.Parameters.AddWithValue("@productId", productId);
                    Int32 Count = (Int32)command.ExecuteScalar();
                    Console.WriteLine("Count: " + Count);
                    CloseConnection();

                    if (Count == 1)
                    {
                        OpenConnection();
                        string queryUpdate = @"Update ShoppingCarts set amount=amount + 1 where user_id=@userId and product_id=@productId";
                        SqlCommand commandUpdate = new SqlCommand(queryUpdate, SqlConnection);
                        commandUpdate.Parameters.AddWithValue("@userId", loggedUser.Id);
                        commandUpdate.Parameters.AddWithValue("@productId", productId);
                        commandUpdate.ExecuteNonQuery();
                        CloseConnection();
                    }
                    else
                    {
                        OpenConnection();
                        string queryInsert = @"Insert into ShoppingCarts(user_id,product_id,amount) values(@userId,@productId,@amount)";
                        SqlCommand commandInsert = new SqlCommand(queryInsert, SqlConnection);
                        commandInsert.Parameters.AddWithValue("@userId", loggedUser.Id);
                        commandInsert.Parameters.AddWithValue("@productId", productId);
                        commandInsert.Parameters.AddWithValue("@amount", 1);
                        commandInsert.ExecuteNonQuery();
                        CloseConnection();
                    }
                    MessageBox.Show("Product added to shopping cart.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void removeShoppingCart(int productId)
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string query = @"Delete from ShoppingCarts where user_id=@userId and product_id=@productId";
                    SqlCommand command = new SqlCommand(query, SqlConnection);
                    command.Parameters.AddWithValue("@userId", loggedUser.Id);
                    command.Parameters.AddWithValue("@productId", productId);
                    command.ExecuteNonQuery();
                    CloseConnection();
                    MessageBox.Show("Product has been removed from shopping cart.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void removeAllProductsShoppingCart()
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string query = @"Delete from ShoppingCarts where user_id=@userId";
                    SqlCommand command = new SqlCommand(query, SqlConnection);
                    command.Parameters.AddWithValue("@userId", loggedUser.Id);
                    command.ExecuteNonQuery();
                    CloseConnection();
                    MessageBox.Show("All products has been removed from shopping cart.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void setShoppingCartProductAmount(int productId, int amount)
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string queryUpdate = @"Update ShoppingCarts set amount=@amount where user_id=@userId and product_id=@productId";
                    SqlCommand commandUpdate = new SqlCommand(queryUpdate, SqlConnection);
                    commandUpdate.Parameters.AddWithValue("@amount", amount);
                    commandUpdate.Parameters.AddWithValue("@userId", loggedUser.Id);
                    commandUpdate.Parameters.AddWithValue("@productId", productId);
                    commandUpdate.ExecuteNonQuery();
                    CloseConnection();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public Dictionary<Product, int> getShoppingCartProducts()
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string query = @"Select * from ShoppingCarts where user_id=" + loggedUser.Id.ToString();
                    SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                    DataTable dataTable = new DataTable();
                    command.Fill(dataTable);
                    CloseConnection();
                    ShoppingCart.Clear();
                    foreach (DataRow item in dataTable.Rows)
                    {
                        Product product = getProduct(int.Parse(item["product_id"].ToString()));
                        ShoppingCart.Add(product, int.Parse(item["amount"].ToString()));
                    }

                    return ShoppingCart;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return null;
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        #endregion

        #region About User

        public int registerUser(User newUser)
        {
            try
            {
                OpenConnection();
                string query = @"Insert into Users(username,email,phone_number,address,password,profile_image_path,rank) values(@username,@email,@phoneNumber,@address,@password,@profileImagePath,@rank) Select CAST(scope_identity() AS int)";
                SqlCommand command = new SqlCommand(query, SqlConnection);
                command.Parameters.AddWithValue("@username", newUser.UserName);
                command.Parameters.AddWithValue("@email", newUser.Email);
                command.Parameters.AddWithValue("@phoneNumber", newUser.PhoneNumber);
                command.Parameters.AddWithValue("@address", newUser.Address);
                command.Parameters.AddWithValue("@password", newUser.Password);
                command.Parameters.AddWithValue("@profileImagePath", newUser.ProfileImage);
                command.Parameters.AddWithValue("@rank", UserRank.User);
                int registeredUserId = (int)command.ExecuteScalar();
                CloseConnection();
                return registeredUserId;
            }
            catch (SqlException error)
            {
                switch (error.Number)
                {
                    case 2601:
                        MessageBox.Show("This username or email is already exists!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    default:
                        MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
            return -1;
        }

        public bool loginUser(string email, string password)
        {
            try
            {
                OpenConnection();
                string query = @"Select * from Users where email='" + email + "' and password='" + password + "'";
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                DataTable dataTable = new DataTable();
                command.Fill(dataTable);
                CloseConnection();

                if (dataTable.Rows.Count == 1)
                {
                    DataRow customerData = dataTable.Rows[0];
                    loggedUser = new User
                    (
                        int.Parse(customerData["id"].ToString()),
                        customerData["username"].ToString(),
                        customerData["email"].ToString(),
                        customerData["phone_number"].ToString(),
                        customerData["address"].ToString(),
                        customerData["password"].ToString(),
                        customerData["profile_image_path"].ToString(),
                        (UserRank)customerData["rank"]
                    );
                    isUserLogged = true;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool changePhoneNumber(string phoneNumber)
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string queryUpdate = @"Update Users set phone_number=@phone_number where id=@userID";
                    SqlCommand commandUpdate = new SqlCommand(queryUpdate, SqlConnection);
                    commandUpdate.Parameters.AddWithValue("@phone_number", phoneNumber);
                    commandUpdate.Parameters.AddWithValue("@userID", loggedUser.Id);
                    commandUpdate.ExecuteNonQuery();
                    CloseConnection();
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        public bool changeAddress(string address)
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string queryUpdate = @"Update Users set address=@address where id=@userID";
                    SqlCommand commandUpdate = new SqlCommand(queryUpdate, SqlConnection);
                    commandUpdate.Parameters.AddWithValue("@address", address);
                    commandUpdate.Parameters.AddWithValue("@userID", loggedUser.Id);
                    commandUpdate.ExecuteNonQuery();
                    CloseConnection();
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        public bool changePassword(string password)
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string queryUpdate = @"Update Users set password=@password where id=@userID";
                    SqlCommand commandUpdate = new SqlCommand(queryUpdate, SqlConnection);
                    commandUpdate.Parameters.AddWithValue("@password", password);
                    commandUpdate.Parameters.AddWithValue("@userID", loggedUser.Id);
                    commandUpdate.ExecuteNonQuery();
                    CloseConnection();
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        public bool changeProfileImage(string profile_image_name)
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string queryUpdate = @"Update Users set profile_image_path=@profile_image_path where id=@userID";
                    SqlCommand commandUpdate = new SqlCommand(queryUpdate, SqlConnection);
                    commandUpdate.Parameters.AddWithValue("@profile_image_path", profile_image_name);
                    commandUpdate.Parameters.AddWithValue("@userID", loggedUser.Id);
                    commandUpdate.ExecuteNonQuery();
                    CloseConnection();
                    return true;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        public void deleteAccount()
        {
            if (isUserLogged)
            {
                try
                {
                    OpenConnection();
                    string query = @"Delete from Users where id=@userID";
                    SqlCommand command = new SqlCommand(query, SqlConnection);
                    command.Parameters.AddWithValue("@userID", loggedUser.Id);
                    command.ExecuteNonQuery();
                    CloseConnection();
                    MessageBox.Show("Account has been deleted.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LogOutUser();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("You must login!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public User getLoggedUser()
        {
            if (isUserLogged)
            {
                OpenConnection();
                string query = @"Select * from Users where id=" + loggedUser.Id.ToString();
                SqlDataAdapter command = new SqlDataAdapter(query, SqlConnection);
                DataTable dataTable = new DataTable();
                command.Fill(dataTable);
                CloseConnection();

                if (dataTable.Rows.Count == 1)
                {
                    DataRow customerData = dataTable.Rows[0];
                    loggedUser = new User
                    (
                        int.Parse(customerData["id"].ToString()),
                        customerData["username"].ToString(),
                        customerData["email"].ToString(),
                        customerData["phone_number"].ToString(),
                        customerData["address"].ToString(),
                        customerData["password"].ToString(),
                        customerData["profile_image_path"].ToString(),
                        (UserRank)customerData["rank"]
                    );
                }
                return loggedUser;
            }
            return null;
        }

        public void LogOutUser()
        {
            this.loggedUser = null;
            this.isUserLogged = false;
        }

        #endregion
    }
}
