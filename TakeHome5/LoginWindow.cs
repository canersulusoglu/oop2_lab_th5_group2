﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome5
{
    public partial class LoginWindow : Form
    {
        Database db = Database.getInstance();
        Timer timer = new Timer();
        int timeLeft = 1;

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void logInBtn_Click(object sender, EventArgs e)
        {
            string email = emailText.Text;
            string password = passwordText.Text;

            if (emailText.TextLength < 1)
            {
                successLabel.ForeColor = Color.Red;
                successLabel.Text = "Please enter your email address.";
                return;
            }
            else if (passwordText.TextLength < 1)
            {
                successLabel.ForeColor = Color.Red;
                successLabel.Text = "Please enter your password.";
                return;
            }

            bool userExists = db.loginUser(email, password);

            if (userExists)
            {
                successLabel.Text = "Logging In...";
                successLabel.ForeColor = Color.Green;

                timer.Interval = 1000;
                timer.Enabled = true;

                timer.Tick += timer_Tick;
            }

            else
            {
                successLabel.Text = "Your email or password is wrong.";
                successLabel.ForeColor = Color.Red;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timeLeft -= 1;

            if (timeLeft == 0)
            {
                this.Hide();
                StoreWindow storeWindow = new StoreWindow();
                storeWindow.FormClosed += (s, args) =>
                {
                    this.Close();
                };
                storeWindow.Show();
                timer.Stop();
            }
        }

        private void goToSignUpLabel_Click(object sender, EventArgs e)
        {
            this.Hide();
            SignUpWindow signUpWindow = new SignUpWindow();
            signUpWindow.FormClosed += (s, args) =>
            {
                this.Close();
            };
            signUpWindow.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
