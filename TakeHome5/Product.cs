﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TakeHome5
{
    class Product
    {
        private int id = -1;
        private string name = "";
        private float price = 0f;
        private string desc = "";
        private string image_name = "";

        public Product(string name, float price, string description, string imageName)
        {
            this.name = name;
            this.price = price;
            this.desc = description;
            this.image_name = imageName;
        }

        public Product(int id, string name, float price, string description, string imageName)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.desc = description;
            this.image_name = imageName;
        }

        public int Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public float Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Desc
        {
            get { return desc; }
            set { desc = value; }
        }

        public string ImageName
        {
            get { return image_name; }
            set { image_name = value; }
        }

        public string ImageWithPath
        {
            get { return Path.Combine(Program.ProductsImagesDir, image_name); }
        }
    }
}
