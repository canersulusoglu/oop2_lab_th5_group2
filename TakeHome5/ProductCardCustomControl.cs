﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome5
{
    public partial class ProductCardCustomControl : UserControl
    {
        public ProductCardCustomControl()
        {
            InitializeComponent();
        }

        #region Properties

        private string _id;
        private string _name;
        private Image _picture;
        private string _price;

        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                lblName.Text = value;
            }
        }

        public Image Picture
        {
            get { return _picture; }
            set
            {
                _picture = value;
                productImage.Image = value;
            }
        }


        public string Price
        {
            get { return _price; }
            set
            {
                _price = value;
                lblPrice.Text = value;
            }
        }

        #endregion

        private void btnAddCart_Click(object sender, EventArgs e)
        {
            Database.getInstance().addShoppingCart(int.Parse(this.Id));
            MessageBox.Show("Product added to the shopping cart successfully");
        }

        private void productImage_Click(object sender, EventArgs e)
        {
            Product product = Database.getInstance().findProductById(int.Parse(this.Id));

            windowProductDetails details = new windowProductDetails
            {
                Id = product.Id.ToString(),
                Name = product.Name,
                Description = product.Desc,
                Picture = Image.FromFile(product.ImageWithPath),
                Price = product.Price.ToString()
            };
            details.ShowDialog();
        }
    }
}
