﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TakeHome5
{
    static class Program
    {
        public static string ImagesDir = Path.Combine(Environment.CurrentDirectory, @"Images\");
        public static string ProductsImagesDir = Path.Combine(ImagesDir, @"Products\");
        public static string UsersImagesDir = Path.Combine(ImagesDir, @"Users\");

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Create folders before app starts
            if (!Directory.Exists(ImagesDir))
            {
                Directory.CreateDirectory(ImagesDir);
            }

            if (!Directory.Exists(ProductsImagesDir))
            {
                Directory.CreateDirectory(ProductsImagesDir);
            }

            if (!Directory.Exists(UsersImagesDir))
            {
                Directory.CreateDirectory(UsersImagesDir);
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginWindow());
        }
    }
}
