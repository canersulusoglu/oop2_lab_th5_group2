﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace TakeHome5
{
    public partial class SignUpWindow : Form
    {
        Database db = Database.getInstance();
        Timer timer = new Timer();
        int timeLeft = 1;
        int registerStatus;
        string imageSafeName;
        public SignUpWindow()
        {
            InitializeComponent();
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void signUpBtn_Click(object sender, EventArgs e)
        {
            string userName = userNameText.Text;
            string email = emailText.Text;
            string password = passwordText.Text;
            string address = addressText.Text;
            string phoneNumber = phoneText.Text;
            string imageFilePath = profileImageText.Text;

            if (emailText.TextLength < 1 || addressText.TextLength < 1 || userNameText.TextLength < 1 || phoneText.TextLength < 1 || passwordText.TextLength < 1 || profileImageText.TextLength < 1)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Please fill all the areas.";
                return;
            }

            if (userNameText.TextLength < 6)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Username must be more than 6 characters.";
                return;
            }
            else if (!IsValidEmail(email))
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "You must enter a valid email address.";
                return;
            }
            else if (passwordText.TextLength < 6)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Password must be more than 6 characters.";
                return;
            }
            else if (profileImageText.TextLength == 0)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Please choose a profile picture.";
                return;
            }
            else if (phoneText.TextLength == 0 || phoneText.TextLength > 10 || !Regex.IsMatch(phoneNumber, @"^\d+$"))
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Please enter a valid phone number.";
                return;
            }
            else if (addressText.TextLength == 0)
            {
                messageLabel.ForeColor = Color.Red;
                messageLabel.Text = "Please enter your address.";
                return;
            }

            else
            {
                User newUser = new User(userName, email, phoneNumber, address, password, imageSafeName, UserRank.User);
                int registeredUserId = db.registerUser(newUser);
                if (registeredUserId != -1)
                {
                    string folderPath = Path.Combine(Program.UsersImagesDir, registeredUserId.ToString());
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    string imagePath = Path.Combine(folderPath, imageSafeName);
                    File.Copy(profileImageText.Text, imagePath, true);
                    registerStatus = 1;
                }

                successLabel.Text = "Wait please...";
                successLabel.ForeColor = Color.Green;
                messageLabel.Visible = false;

                timer.Interval = 1000;
                timer.Enabled = true;

                timer.Tick += timer_Tick;
            }

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            timeLeft -= 1;

            if (timeLeft < 0)
            {
                messageLabel.Visible = true;
                successLabel.Visible = false;
                if (registerStatus == 1)
                {
                    messageLabel.ForeColor = Color.Green;
                    backToLogInBtn.Visible = true;
                    timer.Stop();
                    registerStatus = 0;
                    MessageBox.Show("Registered succesfully!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                messageLabel.ForeColor = Color.Red;
            }
        }

        private void backToLogInBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.FormClosed += (s, args) =>
            {
                this.Close();
            };
            loginWindow.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chooseImageBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files(*.jpg; *.jpeg *.gif; *.bmp;)|*.jpg;*.jpeg; *.gif;*.bmp;";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                profileImageText.Text = openFileDialog.FileName;
                imageSafeName = openFileDialog.SafeFileName;
            }
        }
    }
}
