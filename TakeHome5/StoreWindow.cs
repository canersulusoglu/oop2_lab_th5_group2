﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeHome5.Properties;

namespace TakeHome5
{
    public partial class StoreWindow : Form
    {
        private readonly windowShoppingCart _shoppingCart;
        private string _productAmount;

        public StoreWindow()
        {
            _shoppingCart = new windowShoppingCart();
            _productAmount = "0";
            InitializeComponent();
            if (Database.getInstance().getLoggedUser().Rank == UserRank.Admin)
            {
                label3.Visible = true;
            }
            else
            {
                label3.Visible = false;
            }
        }


        private void StoreWindow_Load(object sender, EventArgs e)
        {
            ListItems();
        }

        private void ListItems()
        {
            foreach (var item in Database.getInstance().getProducts())
            {
                ProductCardCustomControl product = new ProductCardCustomControl
                {
                    Id = item.Id.ToString(),
                    Name = item.Name,
                    Picture = Image.FromFile(item.ImageWithPath),
                    Price = "$" + item.Price.ToString()

                };
                if (flowLayoutProducts.Controls.Count < 0)
                {
                    flowLayoutProducts.Controls.Clear();
                }
                else
                    flowLayoutProducts.Controls.Add(product);
            }

        }

        private void label3_Click(object sender, EventArgs e)
        {
            _shoppingCart.FormClosed += (s, args) =>
            {
                _shoppingCart.Hide();
            };
            _shoppingCart.ShowDialog();
        }

        private void flowLayoutProducts_MouseMove(object sender, MouseEventArgs e)
        {
            lblShoppingCart.Text = "Shopping Cart (" + Database.getInstance().getShoppingCartProducts().Count.ToString() + ")";
        }

        private void lblExit_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Database.getInstance().LogOutUser();
            this.Hide();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.FormClosed += (s, args) =>
            {
                this.Close();
            };
            loginWindow.Show();
        }

        private void label3_Click_1(object sender, EventArgs e)
        {
            windowAdminPanel adminWindow = new windowAdminPanel();
            adminWindow.ShowDialog();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.FormClosed += (s, args) =>
            {
                this.Close();
            };
            loginWindow.Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            flowLayoutProducts.Controls.Clear();
            ListItems();
        }

        private void btnProducts_Click(object sender, EventArgs e)
        {
            movePanelBtnSide(btnProducts);
            btnChangeInfo.Visible = false;
            btnChangePassword.Visible = false;
            flowLayoutProducts.Controls.Clear();
            ListItems();
        }

        private void movePanelBtnSide(Control button)
        {
            panelBtnSide.Top = button.Top;
            panelBtnSide.Height = button.Height;
        }

        private void addUserControlToPanel(Control c)
        {
            flowLayoutProducts.Controls.Clear();
            flowLayoutProducts.Controls.Add(c);
            
        }

        private void btnProfile_Click(object sender, EventArgs e)
        {
            movePanelBtnSide(btnProfile);
            btnChangeInfo.Visible = true;
            btnChangePassword.Visible = true;
        }

        private void btnChangePassword_Click(object sender, EventArgs e)
        {
            ChangePasswordUserControl changePasswordUserControl = new ChangePasswordUserControl();
            addUserControlToPanel(changePasswordUserControl);
        }

        private void btnChangeInfo_Click(object sender, EventArgs e)
        {
            ChangeInfoUserControl changeInfoUserControl = new ChangeInfoUserControl();
            addUserControlToPanel(changeInfoUserControl);

        }
    }
}
