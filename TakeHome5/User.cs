﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TakeHome5
{
    enum UserRank
    {
        User,
        Admin
    }

    class User
    {
        private int id = -1;
        private string userName;
        private string email;
        private string password;
        private UserRank rank;
        private string phoneNumber;
        private string address;
        private string image_name;

        public User(int id, string userName, string email, string phoneNumber, string address, string password, string imageName, UserRank rank)
        {
            this.id = id;
            this.userName = userName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.address = address;
            this.password = password;
            this.image_name = imageName;
            this.rank = rank;
        }

        public User(string userName, string email, string phoneNumber, string address, string password, string imageName, UserRank rank)
        {
            this.userName = userName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.address = address;
            this.password = password;
            this.image_name = imageName;
            this.rank = rank;
        }

        public int Id   // property
        {
            get { return id; }   // get method         
        }

        public string UserName   // property
        {
            get { return userName; }   // get method         
        }

        public string Email   // property
        {
            get { return email; }   // get method         
        }

        public string Password   // property
        {
            get { return password; }   // get method         
        }

        public UserRank Rank   // property
        {
            get { return rank; }   // get method   
        }

        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public string ProfileImage
        {
            get { return image_name; }
            set { image_name = value; }
        }

        public string ProfileImageWithPath
        {
            get { return Path.Combine(Program.UsersImagesDir, this.id.ToString(), image_name); }
        }
    }
}
