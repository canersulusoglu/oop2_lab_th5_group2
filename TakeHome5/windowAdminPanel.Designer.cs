﻿
namespace TakeHome5
{
    partial class windowAdminPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_add = new System.Windows.Forms.Button();
            this.button_delete = new System.Windows.Forms.Button();
            this.button_update = new System.Windows.Forms.Button();
            this.product_image = new System.Windows.Forms.PictureBox();
            this.text_product_name = new System.Windows.Forms.TextBox();
            this.text_product_price = new System.Windows.Forms.TextBox();
            this.text_product_desc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_exit = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.product_list = new System.Windows.Forms.ListView();
            this.button12 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.product_image)).BeginInit();
            this.SuspendLayout();
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(16, 238);
            this.button_add.Margin = new System.Windows.Forms.Padding(4);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(100, 28);
            this.button_add.TabIndex = 0;
            this.button_add.Text = "Add";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_delete
            // 
            this.button_delete.Location = new System.Drawing.Point(16, 273);
            this.button_delete.Margin = new System.Windows.Forms.Padding(4);
            this.button_delete.Name = "button_delete";
            this.button_delete.Size = new System.Drawing.Size(100, 28);
            this.button_delete.TabIndex = 1;
            this.button_delete.Text = "Delete";
            this.button_delete.UseVisualStyleBackColor = true;
            this.button_delete.Click += new System.EventHandler(this.button_delete_Click);
            // 
            // button_update
            // 
            this.button_update.Location = new System.Drawing.Point(16, 309);
            this.button_update.Margin = new System.Windows.Forms.Padding(4);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(100, 28);
            this.button_update.TabIndex = 2;
            this.button_update.Text = "Update";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // product_image
            // 
            this.product_image.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.product_image.Location = new System.Drawing.Point(919, 85);
            this.product_image.Margin = new System.Windows.Forms.Padding(4);
            this.product_image.Name = "product_image";
            this.product_image.Size = new System.Drawing.Size(300, 270);
            this.product_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.product_image.TabIndex = 4;
            this.product_image.TabStop = false;
            // 
            // text_product_name
            // 
            this.text_product_name.Location = new System.Drawing.Point(251, 47);
            this.text_product_name.Margin = new System.Windows.Forms.Padding(4);
            this.text_product_name.Name = "text_product_name";
            this.text_product_name.Size = new System.Drawing.Size(132, 22);
            this.text_product_name.TabIndex = 5;
            // 
            // text_product_price
            // 
            this.text_product_price.Location = new System.Drawing.Point(251, 85);
            this.text_product_price.Margin = new System.Windows.Forms.Padding(4);
            this.text_product_price.Name = "text_product_price";
            this.text_product_price.Size = new System.Drawing.Size(132, 22);
            this.text_product_price.TabIndex = 6;
            // 
            // text_product_desc
            // 
            this.text_product_desc.Location = new System.Drawing.Point(251, 122);
            this.text_product_desc.Margin = new System.Windows.Forms.Padding(4);
            this.text_product_desc.Name = "text_product_desc";
            this.text_product_desc.Size = new System.Drawing.Size(132, 22);
            this.text_product_desc.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(343, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Online Grocery Store Admin Panel";
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(941, 462);
            this.button_exit.Margin = new System.Windows.Forms.Padding(4);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(100, 28);
            this.button_exit.TabIndex = 9;
            this.button_exit.Text = "Exit";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(731, 15);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(319, 22);
            this.dateTimePicker1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(197, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Enter the name of the product";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 85);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Enter the price of the product";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 122);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(231, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Enter the description of the product";
            // 
            // product_list
            // 
            this.product_list.HideSelection = false;
            this.product_list.Location = new System.Drawing.Point(409, 47);
            this.product_list.Margin = new System.Windows.Forms.Padding(4);
            this.product_list.Name = "product_list";
            this.product_list.Size = new System.Drawing.Size(495, 442);
            this.product_list.TabIndex = 14;
            this.product_list.UseCompatibleStateImageBehavior = false;
            this.product_list.SelectedIndexChanged += new System.EventHandler(this.product_list_SelectedIndexChanged_1);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(118, 171);
            this.button12.Margin = new System.Windows.Forms.Padding(4);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(140, 28);
            this.button12.TabIndex = 15;
            this.button12.Text = "Choose An Image";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // windowAdminPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 553);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.product_list);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_product_desc);
            this.Controls.Add(this.text_product_price);
            this.Controls.Add(this.text_product_name);
            this.Controls.Add(this.product_image);
            this.Controls.Add(this.button_update);
            this.Controls.Add(this.button_delete);
            this.Controls.Add(this.button_add);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1250, 650);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1250, 600);
            this.Name = "windowAdminPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Online Grocery Store Admin Panel";
            this.Load += new System.EventHandler(this.windowAdminPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.product_image)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_delete;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.PictureBox product_image;
        private System.Windows.Forms.TextBox text_product_name;
        private System.Windows.Forms.TextBox text_product_price;
        private System.Windows.Forms.TextBox text_product_desc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView product_list;
        private System.Windows.Forms.Button button12;
    }
}

