﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TakeHome5
{
    public partial class windowAdminPanel : Form
    {
        private OpenFileDialog openFileDialog = new OpenFileDialog();
        private DialogResult FileDialogResult = DialogResult.Cancel;

        public windowAdminPanel()
        {
            InitializeComponent();
        }

        private void windowAdminPanel_Load(object sender, EventArgs e)
        {
            product_list.FullRowSelect = true;
            product_list.GridLines = true;
            product_list.View = View.Details;
            product_list.Columns.Add("Id", 45);
            product_list.Columns.Add("Product Name", 100);
            product_list.Columns.Add("Product Price ($)", 100);
            product_list.Columns.Add("Product Description", 120);

            // Add Data to listview
            foreach (Product item in Database.getInstance().getProducts())
            {
                string[] row = { item.Id.ToString(), item.Name, item.Price.ToString(), item.Desc };
                ListViewItem listViewItem = new ListViewItem(row);
                product_list.Items.Add(listViewItem);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (text_product_name.TextLength == 0 || text_product_price.TextLength == 0 || text_product_desc.TextLength == 0)
            {
                MessageBox.Show("Input can not be empty!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (FileDialogResult == DialogResult.OK)
                {
                    string onlyFileName = openFileDialog.SafeFileName;
                    string destinationPath = Path.Combine(Program.ProductsImagesDir, onlyFileName);
                    File.Copy(openFileDialog.FileName, destinationPath, true);

                    // Add product to list
                    int addedProductId = Database.getInstance().addProduct(new Product(text_product_name.Text, float.Parse(text_product_price.Text), text_product_desc.Text, onlyFileName));

                    // Add product to listview
                    string[] row = { addedProductId.ToString(), text_product_name.Text, text_product_price.Text, text_product_desc.Text };
                    ListViewItem lvi = new ListViewItem(row);
                    product_list.Items.Add(lvi);

                    text_product_name.Text = "";
                    text_product_price.Text = "";
                    text_product_desc.Text = "";
                    FileDialogResult = DialogResult.Cancel;
                    openFileDialog.Reset();
                }
                else
                {
                    MessageBox.Show("Image selection can not be empty!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void button_update_Click(object sender, EventArgs e)
        {
            if (product_list.FocusedItem != null)
            {
                // Update list
                int selectedProductId = int.Parse(product_list.Items[product_list.FocusedItem.Index].SubItems[0].Text);
                Product willUpdate = Database.getInstance().findProductById(selectedProductId);
                willUpdate.Name = text_product_name.Text;
                willUpdate.Price = float.Parse(text_product_price.Text);
                willUpdate.Desc = text_product_desc.Text;
                // Update listview
                product_list.Items[product_list.FocusedItem.Index].SubItems[1].Text = text_product_name.Text;
                product_list.Items[product_list.FocusedItem.Index].SubItems[2].Text = text_product_price.Text;
                product_list.Items[product_list.FocusedItem.Index].SubItems[3].Text = text_product_desc.Text;

                // If image is updating
                if (FileDialogResult == DialogResult.OK)
                {
                    string onlyFileName = openFileDialog.SafeFileName;
                    string destinationPath = Path.Combine(Program.ProductsImagesDir, onlyFileName);
                    File.Copy(openFileDialog.FileName, destinationPath, true);

                    // Delete old image
                    product_image.ImageLocation = null;
                    File.Delete(willUpdate.ImageWithPath);

                    willUpdate.ImageName = onlyFileName;
                    product_image.ImageLocation = willUpdate.ImageWithPath;

                    FileDialogResult = DialogResult.Cancel;
                }

                // Send database request to update product
                Database.getInstance().updateProduct(willUpdate);
            }
            else
            {
                MessageBox.Show("SELECT A PRODUCT TO UPDATE", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            if (product_list.FocusedItem != null)
            {
                // Remove product from list
                int selectedProductId = int.Parse(product_list.Items[product_list.FocusedItem.Index].SubItems[0].Text);
                Product willDelete = Database.getInstance().findProductById(selectedProductId);
                File.Delete(willDelete.ImageWithPath);
                product_image.ImageLocation = null;

                // Remove product from listview
                product_list.Items.RemoveAt(product_list.FocusedItem.Index);

                // Send database request to delete product
                Database.getInstance().deleteProductById(willDelete.Id);
            }
            else
            {
                MessageBox.Show("SELECT A PRODUCT TO DELETE", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            text_product_name.Text = "";
            text_product_price.Text = "";
            text_product_desc.Text = "";
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            DialogResult result=MessageBox.Show("Are you sure you want to exit?","Exit",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (result == DialogResult.Yes) 
            { 
                Application.Exit(); 
            }
        }

        private void product_list_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (product_list.FocusedItem != null && product_list.FocusedItem.Index != -1)
            {
                int selectedProductId = int.Parse(product_list.Items[product_list.FocusedItem.Index].SubItems[0].Text);
                text_product_name.Text = product_list.Items[product_list.FocusedItem.Index].SubItems[1].Text;
                text_product_price.Text = product_list.Items[product_list.FocusedItem.Index].SubItems[2].Text;
                text_product_desc.Text = product_list.Items[product_list.FocusedItem.Index].SubItems[3].Text;
                product_image.ImageLocation = Database.getInstance().findProductById(selectedProductId).ImageWithPath;
            }

        }

        private void button12_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            FileDialogResult = openFileDialog.ShowDialog();
        }
    }
}
