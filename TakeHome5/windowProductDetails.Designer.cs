﻿
namespace TakeHome5
{
    partial class windowProductDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPriceDetails = new System.Windows.Forms.Label();
            this.lblNameDetails = new System.Windows.Forms.Label();
            this.lblDescriptionDetails = new System.Windows.Forms.Label();
            this.productImage = new System.Windows.Forms.PictureBox();
            this.btnAddCartDetails = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.productImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPriceDetails
            // 
            this.lblPriceDetails.AutoSize = true;
            this.lblPriceDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPriceDetails.Location = new System.Drawing.Point(310, 262);
            this.lblPriceDetails.Name = "lblPriceDetails";
            this.lblPriceDetails.Size = new System.Drawing.Size(56, 25);
            this.lblPriceDetails.TabIndex = 7;
            this.lblPriceDetails.Text = "Price";
            // 
            // lblNameDetails
            // 
            this.lblNameDetails.AutoSize = true;
            this.lblNameDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblNameDetails.Location = new System.Drawing.Point(68, 33);
            this.lblNameDetails.Name = "lblNameDetails";
            this.lblNameDetails.Size = new System.Drawing.Size(94, 32);
            this.lblNameDetails.TabIndex = 6;
            this.lblNameDetails.Text = "Name";
            // 
            // lblDescriptionDetails
            // 
            this.lblDescriptionDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDescriptionDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblDescriptionDetails.Location = new System.Drawing.Point(406, 71);
            this.lblDescriptionDetails.Name = "lblDescriptionDetails";
            this.lblDescriptionDetails.Size = new System.Drawing.Size(275, 288);
            this.lblDescriptionDetails.TabIndex = 5;
            this.lblDescriptionDetails.Text = "Description";
            // 
            // productImage
            // 
            this.productImage.Location = new System.Drawing.Point(71, 71);
            this.productImage.Name = "productImage";
            this.productImage.Size = new System.Drawing.Size(295, 188);
            this.productImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.productImage.TabIndex = 4;
            this.productImage.TabStop = false;
            // 
            // btnAddCartDetails
            // 
            this.btnAddCartDetails.Location = new System.Drawing.Point(145, 318);
            this.btnAddCartDetails.Name = "btnAddCartDetails";
            this.btnAddCartDetails.Size = new System.Drawing.Size(136, 41);
            this.btnAddCartDetails.TabIndex = 8;
            this.btnAddCartDetails.Text = "Add to Cart";
            this.btnAddCartDetails.UseVisualStyleBackColor = true;
            this.btnAddCartDetails.Click += new System.EventHandler(this.btnAddCart_Click);
            // 
            // windowProductDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 417);
            this.Controls.Add(this.btnAddCartDetails);
            this.Controls.Add(this.lblPriceDetails);
            this.Controls.Add(this.lblNameDetails);
            this.Controls.Add(this.lblDescriptionDetails);
            this.Controls.Add(this.productImage);
            this.Name = "windowProductDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "windowProductDetails";
            ((System.ComponentModel.ISupportInitialize)(this.productImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPriceDetails;
        private System.Windows.Forms.Label lblNameDetails;
        private System.Windows.Forms.Label lblDescriptionDetails;
        private System.Windows.Forms.PictureBox productImage;
        private System.Windows.Forms.Button btnAddCartDetails;
    }
}