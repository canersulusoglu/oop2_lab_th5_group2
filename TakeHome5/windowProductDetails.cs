﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome5
{
    public partial class windowProductDetails : Form
    {
        public windowProductDetails()
        {
            InitializeComponent();
        }

        #region Properties

        private string _id;
        private string _name;
        private Image _picture;
        private string _price;
        private string _description;

        public string Id
        {
            get { return _id; }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                lblNameDetails.Text = value;
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                lblDescriptionDetails.Text = value;
            }
        }
        public Image Picture
        {
            get { return _picture; }
            set
            {
                _picture = value;
                productImage.Image = value;
            }
        }


        public string Price
        {
            get { return _price; }
            set
            {
                _price = value;
                lblPriceDetails.Text = value;
            }
        }


        #endregion

        private void btnAddCart_Click(object sender, EventArgs e)
        {
            Database.getInstance().addShoppingCart(int.Parse(this.Id));
            
            MessageBox.Show("Product added to the shopping cart successfully");
        }
    }
}
