﻿namespace TakeHome5
{
    partial class windowShoppingCart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sc = new System.Windows.Forms.ListView();
            this.change = new System.Windows.Forms.Button();
            this.update_box = new System.Windows.Forms.TextBox();
            this.delete = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label_total_price = new System.Windows.Forms.Label();
            this.removeAllItems = new System.Windows.Forms.Button();
            this.confirmShoppingCartButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // sc
            // 
            this.sc.HideSelection = false;
            this.sc.Location = new System.Drawing.Point(264, 69);
            this.sc.Margin = new System.Windows.Forms.Padding(4);
            this.sc.Name = "sc";
            this.sc.Size = new System.Drawing.Size(607, 436);
            this.sc.TabIndex = 1;
            this.sc.UseCompatibleStateImageBehavior = false;
            // 
            // change
            // 
            this.change.Location = new System.Drawing.Point(13, 69);
            this.change.Margin = new System.Windows.Forms.Padding(4);
            this.change.Name = "change";
            this.change.Size = new System.Drawing.Size(229, 28);
            this.change.TabIndex = 2;
            this.change.Text = "change the amount of a product";
            this.change.UseVisualStyleBackColor = true;
            this.change.Click += new System.EventHandler(this.change_Click);
            // 
            // update_box
            // 
            this.update_box.Location = new System.Drawing.Point(14, 105);
            this.update_box.Margin = new System.Windows.Forms.Padding(4);
            this.update_box.Name = "update_box";
            this.update_box.Size = new System.Drawing.Size(228, 22);
            this.update_box.TabIndex = 3;
            // 
            // delete
            // 
            this.delete.Location = new System.Drawing.Point(14, 158);
            this.delete.Margin = new System.Windows.Forms.Padding(4);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(228, 28);
            this.delete.TabIndex = 4;
            this.delete.Text = "Remove a product";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 341);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 17);
            this.label3.TabIndex = 8;
            // 
            // label_total_price
            // 
            this.label_total_price.AutoSize = true;
            this.label_total_price.Location = new System.Drawing.Point(4, 372);
            this.label_total_price.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_total_price.Name = "label_total_price";
            this.label_total_price.Size = new System.Drawing.Size(46, 17);
            this.label_total_price.TabIndex = 9;
            this.label_total_price.Text = "label4";
            // 
            // removeAllItems
            // 
            this.removeAllItems.Location = new System.Drawing.Point(15, 222);
            this.removeAllItems.Margin = new System.Windows.Forms.Padding(4);
            this.removeAllItems.Name = "removeAllItems";
            this.removeAllItems.Size = new System.Drawing.Size(228, 28);
            this.removeAllItems.TabIndex = 10;
            this.removeAllItems.Text = "Remove all products";
            this.removeAllItems.UseVisualStyleBackColor = true;
            this.removeAllItems.Click += new System.EventHandler(this.removeAllItems_Click);
            // 
            // confirmShoppingCartButton
            // 
            this.confirmShoppingCartButton.Location = new System.Drawing.Point(13, 477);
            this.confirmShoppingCartButton.Margin = new System.Windows.Forms.Padding(4);
            this.confirmShoppingCartButton.Name = "confirmShoppingCartButton";
            this.confirmShoppingCartButton.Size = new System.Drawing.Size(228, 28);
            this.confirmShoppingCartButton.TabIndex = 11;
            this.confirmShoppingCartButton.Text = "Confirm shopping cart.";
            this.confirmShoppingCartButton.UseVisualStyleBackColor = true;
            this.confirmShoppingCartButton.Click += new System.EventHandler(this.confirmShoppingCartButton_Click);
            // 
            // Shopping_cart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.confirmShoppingCartButton);
            this.Controls.Add(this.removeAllItems);
            this.Controls.Add(this.label_total_price);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.update_box);
            this.Controls.Add(this.change);
            this.Controls.Add(this.sc);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Shopping_cart";
            this.Text = "Shopping cart";
            this.Load += new System.EventHandler(this.Shopping_cart_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView sc;
        private System.Windows.Forms.Button change;
        private System.Windows.Forms.TextBox update_box;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_total_price;
        private System.Windows.Forms.Button removeAllItems;
        private System.Windows.Forms.Button confirmShoppingCartButton;
    }
}