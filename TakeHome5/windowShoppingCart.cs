﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeHome5
{
    public partial class windowShoppingCart : Form
    {
        private float total_price;

        public windowShoppingCart()
        {
            InitializeComponent();
            total_price = 0;
            sc.FullRowSelect = true;
            sc.GridLines = true;
            sc.View = View.Details;
            sc.Columns.Add("product ID", 75);
            sc.Columns.Add("product", 70);
            sc.Columns.Add("price", 70);
            sc.Columns.Add("description", 100);
            sc.Columns.Add("amount of products", 130);
        }

        private void updateProductList()
        {
            sc.Items.Clear();
            total_price = 0;
            foreach (var product in Database.getInstance().getShoppingCartProducts())
            {
                int amount = product.Value;
                total_price += (product.Key.Price) * amount;
                string[] listProducts = { product.Key.Id.ToString(), product.Key.Name, product.Key.Price.ToString(), product.Key.Desc, amount.ToString() };
                ListViewItem listViewItem = new ListViewItem(listProducts);
                sc.Items.Add(listViewItem);
            }
            label_total_price.Text = "Total price is: " + total_price + "$";
        }

        private void Shopping_cart_Load(object sender, EventArgs e)
        {
            updateProductList();
        }

        private void change_Click(object sender, EventArgs e)
        {
            if (sc.FocusedItem != null)
            {
                if (update_box.TextLength == 0)
                {
                    MessageBox.Show("Input can not be empty!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (int.Parse(update_box.Text) <= 0)
                {
                    MessageBox.Show("Input can not be zero or below!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Database.getInstance().setShoppingCartProductAmount(int.Parse(sc.Items[sc.FocusedItem.Index].SubItems[0].Text), Int16.Parse(update_box.Text));

                    sc.Items[sc.FocusedItem.Index].SubItems[4].Text = update_box.Text.ToString();
                    updateProductList();

                }
            }
            else
            {
                MessageBox.Show("Item not selected!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            if (sc.FocusedItem != null)
            {
                Database.getInstance().removeShoppingCart(int.Parse(sc.Items[sc.FocusedItem.Index].SubItems[0].Text));
                sc.Items.RemoveAt(sc.FocusedItem.Index);
                updateProductList();

            }
        }

        private void removeAllItems_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to delete all items ?", "Confirm Delete!!", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                Database.getInstance().removeAllProductsShoppingCart();
                updateProductList();
            }
        }

        private void confirmShoppingCartButton_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure to confirm shopping cart ?", "Confirm!!", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                foreach (var product in Database.getInstance().getShoppingCartProducts())
                {
                    Database.getInstance().removeShoppingCart(product.Key.Id);
                }
                updateProductList();
            }
        }
    }
}
